#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
void *push_to_stack(char **sp, void *data, size_t data_size, char *end);
void align_pointer(char **sp);
void advance_stack_pointer(char **sp);

#endif /* userprog/process.h */
